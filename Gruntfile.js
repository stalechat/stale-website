module.exports = function(grunt) {

	grunt.initConfig({
		jshint: {
			options: {
			},
			build: ['Gruntfile.js', 'js/main.js']
		},
		less: {
			development: {
				options: {
					paths: ['css/*']
				},
				files: {
					'css/main.css': 'css/main.less'
				},
			},
		},
		concat: {
			javascript: {
				options: {
					separator: ';'
				},
				files: {
					'js/script.js' : 'js/main.js'
				}
			},
			css: {
				options: {
					separator: ';'
				},
				files: {
					'css/style.css' : ['css/reset.css', 'css/main.css']
				},
			}
		},
		uglify: {
			dist: {
				files: {
					'js/script.min.js': 'js/script.js'
				}
			}
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'css/style.min.css': 'css/style.css'
				}
			}
		},
		watch: {
			options: {
				livereload: true,
			},
			css: {
				files: 'css/*.less',
				tasks: ['styling'],
			},
			scripts: { 
				files: 'js/*.js', 
				tasks: ['scripting'] 
			} 
		}
	});


	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['jshint', 'less', 'concat', 'uglify', 'cssmin']);
	grunt.registerTask('scripting', ['jshint', 'concat:javascript', 'uglify']);
	grunt.registerTask('styling', ['less', 'concat:css', 'cssmin']);

};
